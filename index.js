// ---> ACTIVITY 20 SOLUTION <---

let number = Number(prompt("Give me a number"))
console.log("The number you provided is " +  number);
	
for(number; number !== 0; number--){
	
	if(number <= 50){

		console.log("The current value is at 50. Terminating the loop.");

		break;

	};

	if(number % 10 === 0){

		console.log("The nubmer is divisible by 10. Skipping the number.");

		continue;
	};

	if(number % 5 === 0){

		console.log(number);

		continue;
	};

};


/*
	b. Stretch Goals

	>> Create a variable that will contain the string supercalifragilisticexpialidocious.

	>> Create another variable with an emtpy string as a value that will store the consonants from the string.

	>> Create for Loop that will iterate through the individual letters of the string based on it’s length.

	>> Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

	>> Create an else statement that will add the letter to the second variable.
		>> use addition assignment operator
		>> use the concept of index to add the specific letter in the second variable

	>> After the loop is complete, print the filtered string without the vowels

*/

// ---> ACTIVITY 20 SOLUTION STRETCH GOAL <---

let string1 = "supercalifragilisticexpialidocious"
let string2 = ""

for(let i = 0; i <string1.length; i++){

	if(
		string1[i].toLowerCase() == "a" ||
		string1[i].toLowerCase() == "e" ||
		string1[i].toLowerCase() == "i" ||
		string1[i].toLowerCase() == "o" ||
		string1[i].toLowerCase() == "u" 
	){
			continue;

	} else {

		string2 = string2 + string1[i];

	}

};
console.log(string1)
console.log(string2)
